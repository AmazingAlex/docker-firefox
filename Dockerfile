FROM debian:buster
LABEL maintainer="alex bachmer"

# DEFINE BUILDARGUMENTS
ARG lUSER="alex"
ARG uid=1000
ARG gid=$uid

# UPDATE AND INSTALL DEPENDENCIES
ENV DEBIAN_FRONTEND=noninteractive                                              
ADD firefox_dep.lst .
RUN apt-get update && apt-get upgrade -y &&\
    cat firefox_dep.lst | xargs apt-get install -y

# CREATE USER
RUN groupadd -g 1000 $lUSER && \
    useradd -m -d /home/$lUSER -u $uid -g $gid -G sudo $lUSER && \
    mkdir -p /etc/sudoers.d && \                                     
    echo "$lUSER ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$lUSER && \              
    chmod 0440 /etc/sudoers.d/$lUSER                           

# INSTALL FIREFOX
RUN wget "https://download.mozilla.org/?product=firefox-latest&os=linux64&lang=de" \
    -O firefox_latest.tar.bz2 && \
    tar xfvj firefox_latest.tar.bz2 && \
    mv firefox/ /opt/

# ENABLE SOUND
RUN echo enable-shm=no >> /etc/pulse/client.conf
ENV PULSE_SERVER=unix:/run/user/1000/pulse/native

# RUN FIREFOX AS MY OWN TESTUSER
USER $lUSER
ENV HOME /home/$lUSER

# EXECUTE FIREFOX
CMD /opt/firefox/firefox
