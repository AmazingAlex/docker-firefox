Build container:
```bash
docker build -t firefox .
```

Run docker container with:
```bash
docker run -it --rm --name=firefox -e DISPLAY=$DISPLAY \                        
         -e XAUTHORITY=$XAUTHORITY \                                             
         -v /dev/shm:/dev/shm \                                                  
         -v /tmp/.X11-unix:/tmp/.X11-unix \                                      
         -v /home/$USER/docker/firefox/:/tmp \
         -v /home/$USER/Downloads:/home/$USER \                     
         -v $XDG_RUNTIME_DIR/pulse/native:$XDG_RUNTIME_DIR/pulse/native \        
         -v /etc/hosts:/etc/hosts:ro                                                
         -d firefox
```
